@extends('fontend_home')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li class="active">Giỏ hàng</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <?php
                $carts = Cart::content();
                ?>
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Hình ảnh</td>
                        <td class="description"> Mô tả</td>
                        <td class="price">Giá</td>
                        <td class="quantity">Số lượng</td>
                        <td class="total">Tổng tiền</td>
                        <td></td>
                    </tr>
                    </thead>
                    @foreach($carts as $cart)
                        <tbody>
                        <tr>
                            <td class="cart_product">
                                <a href=""><img width="100px" height="100px"
                                                src="{{URL::asset('uploads/product/'. $cart->options->image)}}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$cart->name}}</a></h4>
                                <p>{{$cart->id}}</p>
                            </td>
                            <td class="cart_price">
                                <p>{{$cart->price}}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <form action="{{URL::to('/update-cart-quality')}}" method="POST">
                                        {{csrf_field()}}
                                        <input class="cart_quantity_input" type="number" name="quatity" size="1"
                                               value="{{$cart->qty}}"  >
                                        <input type="hidden" value="{{$cart->rowId}}" name="row_id">
                                        <input type="submit" value="Cập nhật" name="update_qty"
                                               class="btn  btn-default btn-sm">
                                    </form>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                    <?php
                                    $cart_total = $cart->price * $cart->qty;
                                    echo $cart_total;
                                    ?>
                                </p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href="{{URL::to('/delete_card/'.$cart->rowId)}}"><i
                                        class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->
    <section id="do_action">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Tổng <span>{{Cart::total()}}</span></li>
                            <li>Thuế <span>{{Cart::tax()}}</span></li>
                            <li>Phí vận chuyển <span>Free</span></li>
                            <li>Thành tiền <span>{{Cart::total()+Cart::tax()}}</span></li>
                        </ul>
                        <?php
                        $customer_id = Session::get('customer_id');
                        if ($customer_id != NULL) {
                        ?>
                        <a class="btn btn-default check_out" href="{{URL::to('/check_out')}}">Thanh toán</a>

                        <?php
                        }else{
                        ?>
                        <a class="btn btn-default check_out" href="{{URL::to('/login-checkout')}}">Thanh toán</a>
                    <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
@endsection()
