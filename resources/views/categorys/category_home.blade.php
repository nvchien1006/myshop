@extends('fontend_home')
@section('content')
    <div class="features_items"><!--features_items-->
        <h2 class="title text-center">Danh mục sản phẩm</h2>
        @foreach($cate_by_id as $key=>$product)
        <div class="col-sm-4">
            <div class="product-image-wrapper">
                <div class="single-products">
                    <div class="productinfo text-center">
                        <img  alt="Images" width="100px" height="300px"
                              src="{{URL::asset('/uploads/product/' . $product->product_img)}}"/>
                        <h2>{{$product->product_price." VNĐ"}}</h2>
                        <p>{{$product->product_name}}</p>
                        <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart">
                            </i>Thêm vào giỏ hàng
                        </a>
                    </div>
                </div>
                <div class="choose">
                    <ul class="nav nav-pills nav-justified">
                        <li><a href="#"><i class="fa fa-plus-square"></i>Yêu thích</a></li>
                        <li><a href="#"><i class="fa fa-plus-square"></i>So sánh</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @endforeach
    </div><!--features_items-->
@endsection
