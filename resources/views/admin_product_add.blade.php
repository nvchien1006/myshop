@extends('admin_layout')

@section('admin_content')
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Thêm sản phẩm
        </header>
        <?php
        $mgs = Session::get('message');
        if ($mgs) {
            echo '<span class ="login_error">' . $mgs . '</span>';
            Session::put('message',null);
        }
        ?>
        <div class="panel-body">
            <div class="position-center">
                <form role="form" enctype="multipart/form-data" action="{{URL::to('/product_save')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên sản phẩm</label>
                        <input name="product_name" type="text" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Giá sản phẩm</label>
                        <input name="product_price" type="text" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Ảnh sản phẩm</label>
                        <input name="product_img" type="file" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả sản phẩm</label>
                        <textarea name="product_desc" style="resize: none" rows="8" class="form-control"  placeholder="Mô tả sản phẩm"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Nội dung sản phẩm</label>
                        <textarea name="product_content" style="resize: none" rows="8" class="form-control"  placeholder="Nội dung sản phẩm"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                        <div class="checkbox">
                            <select name="cate_id" class="form-control input-sm m-bot15">
                                @foreach($cates as $key => $cate)
                                <option value="{{$cate->id}}">{{$cate->category_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Thương hiệu</label>
                        <div class="checkbox">
                            <select name="brand_id" class="form-control input-sm m-bot15">
                                @foreach($brands as $key=>$brand)
                                <option value="{{$brand->id}}">{{$brand->brand_name}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputPassword1">Hiển thị</label>
                        <div class="checkbox">
                            <select name="product_status" class="form-control input-sm m-bot15">
                                <option value="0">Ẩn</option>
                                <option value="1">Hiển thị</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" name ="category_add" class="btn btn-info">Thêm sản phẩm</button>
                </form>
            </div>

        </div>
    </section>
</div>
@endsection
