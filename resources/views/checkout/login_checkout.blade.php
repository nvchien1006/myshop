@extends('fontend_home')
@section('content')
<div class="container">
<div class="row">
<div class="col-sm-4 col-sm-offset-1">
    <div class="login-form"><!--login form-->
        <h2>Đăng nhập vào tài khoản của bạn</h2>
        <form action="{{URL::to('/login-customer')}}" method="POST">
            {{csrf_field()}}
            <input type="text" name ="mail" placeholder="Tài khoản"/>
            <input type="password" name="pas" placeholder="Mật khẩu"/>
            <span><input type="checkbox" class="checkbox">Ghi nhớ đăng nhập</span>
            <button type="submit" class="btn btn-default">Đăng nhập</button>
        </form>
    </div><!--/login form-->
</div>
<div class="col-sm-1">
    <h2 class="or">OR</h2>
</div>
<div class="col-sm-4">
    <div class="signup-form"><!--sign up form-->
        <h2>New User Signup!</h2>
        <form action="{{URL::to('/add_customer')}}" method="POST">
            {{csrf_field()}}
            <input type="text" name="name" placeholder="Tên"/>
            <input type="email" name="email" placeholder="Địa chỉ email"/>
            <input type="password" name="pass" placeholder="Mật khẩu"/>
            <input type="text" name="phone" placeholder="Số điện thoại"/>
            <button type="submit" class="btn btn-default">Đăng ký</button>
        </form>
    </div><!--/sign up form-->
</div>
</div>
</div>

@endsection
