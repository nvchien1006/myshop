@extends('fontend_home')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="{{URL::to('/')}}">Trang chủ</a></li>
                    <li class="active">Thanh toán giỏ hàng</li>
                </ol>
            </div><!--/breadcrums-->

            <div class="review-payment">
                <h2>Xem lại giỏ hàng</h2>
            </div>

            <div class="table-responsive cart_info">
                <?php
                $carts = Cart::content();
                ?>
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Hình ảnh</td>
                        <td class="description"> Mô tả</td>
                        <td class="price">Giá</td>
                        <td class="quantity">Số lượng</td>
                        <td class="total">Tổng tiền</td>
                        <td></td>
                    </tr>
                    </thead>
                    @foreach($carts as $cart)
                        <tbody>
                        <tr>
                            <td class="cart_product">
                                <a href=""><img width="100px" height="100px"
                                                src="{{URL::asset('uploads/product/'. $cart->options->image)}}" alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{$cart->name}}</a></h4>
                                <p>{{$cart->id}}</p>
                            </td>
                            <td class="cart_price">
                                <p>{{$cart->price}}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <form action="{{URL::to('/update-cart-quality')}}" method="POST">
                                        {{csrf_field()}}
                                        <input class="cart_quantity_input" type="number" name="quatity" size="1"
                                               value="{{$cart->qty}}"  >
                                        <input type="hidden" value="{{$cart->rowId}}" name="row_id">
                                        <input type="submit" value="Cập nhật" name="update_qty"
                                               class="btn  btn-default btn-sm">
                                    </form>
                                </div>
                            </td>
                            <td class="cart_total">
                                <p class="cart_total_price">
                                    <?php
                                    $cart_total = $cart->price * $cart->qty;
                                    echo $cart_total;
                                    ?>
                                </p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete" href="{{URL::to('/delete_card/'.$cart->rowId)}}"><i
                                        class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
            <h4 style="margin: 40px 0px; font-size:20px ">Chọn hình thức thanh toán</h4>
            <form method="POST" action="{{URL::to('/order-place')}}">
            <div class="payment-options">
                {{csrf_field()}}
					<span>
						<label><input name="payment_option" value="1" type="checkbox"> Trả bằng thẻ ATM</label>
					</span>
                    <span>
						<label><input name="payment_option" value="2" type="checkbox"> Nhận tiền mặt</label>
					</span>
                <input type="submit" value="Đặt hàng" name="send_order_place" class="btn btn-primary btn-sm"/>
            </div>
            </form>
        </div>
    </section>
@endsection
