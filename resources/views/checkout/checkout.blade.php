@extends('fontend_home')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Trang chủ</a></li>
                    <li class="active">Thanh toán giỏ hàng</li>
                </ol>
            </div><!--/breadcrums-->
            <div class="register-req">
                <p>Làm ơn đăng ký hoặc đăng nhập để thanh toán và xem lại lịch sử mua hàng</p>
            </div><!--/register-req-->

            <div class="shopper-informations">
                <div class="col-sm-40 clearfix">
                    <div class="bill-to">
                        <p>Thông tin gửi hàng</p>
                        <div class="form-one">
                            <form method="POST" action="{{URL::to('/save_checkout_customer')}}">
                                {{csrf_field()}}
                                <input type="text" name="shiping_mail" placeholder="Email">
                                <input type="text" name="shiping_name" placeholder="Họ và tên">
                                <input type="text" name="shiping_address" placeholder="Địa chỉ">
                                <input type="text" name="shiping_phone" placeholder="Số điện thoại">
                                <textarea name="shiping_note" placeholder="Ghi chú đơn hàng của bạn" rows="10"></textarea>
                                <input type="submit" value="Gửi" name="save_order" class="btn btn-primary btn-sm">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> <!--/#cart_items-->
@endsection
