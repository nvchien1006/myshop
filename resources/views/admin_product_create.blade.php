@extends('admin_layout')

@section('admin_content')
<div class="col-lg-12">
    <section class="panel">
        <header class="panel-heading">
            Thêm danh mục sản phẩm
        </header>
        <?php
        $mgs = Session::get('message');
        if ($mgs) {
            echo '<span class ="login_error">' . $mgs . '</span>';
            Session::put('message',null);
        }
        ?>
        <div class="panel-body">
            <div class="position-center">
                <form role="form" action="{{URL::to('/save_product')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="exampleInputEmail1">Tên danh mục</label>

                        <input name="category_name" type="text" class="form-control"  placeholder="Tên danh mục">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Mô tả danh mục</label>
                        <textarea name="category_desc" style="resize: none" rows="8" class="form-control"  placeholder="Mô tả danh mục">

                        </textarea>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Hiển thị</label>
                        <div class="checkbox">
                            <select name="category_status" class="form-control input-sm m-bot15">
                                <option value="0">Ẩn</option>
                                <option value="1">Hiển thị</option>
                            </select>
                        </div>
                    </div>
                    <button type="submit" name ="category_add" class="btn btn-info">Thêm danh mục</button>
                </form>
            </div>

        </div>
    </section>

</div>
@endsection
