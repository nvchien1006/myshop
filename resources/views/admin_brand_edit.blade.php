@extends('admin_layout')

@section('admin_content')
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Sửa nhãn sản phẩm
            </header>
            <div class="panel-body">

                <div class="position-center">
                    @foreach($brandInfor as $key =>$value)
                        <form role="form" action="{{URL::to('/updateBrand/'.$value->id)}}"
                              method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên nhãn hàng</label>

                                <input name="brand_name" type="text" class="form-control"
                                       value="{{$value ->brand_name}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Mô tả nhãn hàng</label>
                                <textarea name="brand_desc" style="    resize: none" rows="8"
                                          class="form-control">{{$value ->brand_des}}</textarea>
                            </div>
                            <button type="submit" name="category_add" class="btn btn-info">Cập nhật danh mục</button>
                        </form>
                    @endforeach
                </div>

            </div>
        </section>

    </div>
@endsection
