@extends('admin_layout')
@section('admin_content')

    <div class="panel panel-default">
        <div class="panel-heading">
           Thông tin khách hàng
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
                <thead>
                <tr>
                    <th>Tên khác hàng</th>
                    <th>Số điện thoại</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$order_detail ->customer_name}}</td>
                        <td>{{$order_detail ->customer_phone}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
            Thông tin vận chuyển
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
                <thead>
                <tr>
                    <th>Tên người vận chuyển</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$order_detail ->shiping_name}}</td>
                        <td>{{$order_detail ->shiping_address}}</td>
                        <td>{{$order_detail ->shiping_phone}}</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="panel panel-default">
        <div class="panel-heading">
             Chi tiết đơn hàng
        </div>
        <div class="table-responsive">
            <table class="table table-striped b-t b-light">
                <thead>
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Giá</th>
                    <th>Tổng tiền</th>
                </tr>
                </thead>
                <tbody>
                <tbody>
                    <tr>

                        <td>{{$order_detail ->product_name}}</td>
                        <td>{{$order_detail ->product_sales_quality}}</td>
                        <td>{{$order_detail ->product_price}}</td>
                        <td>{{$order_detail ->product_price*$order_detail ->product_sales_quality}}</td>
                    </tr>

                </tbody>
            </table>
        </div>
    </div>


@endsection
