<?php

?>


@extends('layout.app')
@section('content')
    <div>
        {{--        @include('errors.503')--}}
        <form action="{{url('task')}}" method="post">
            {{ csrf_field() }}

            <label> Task</label>
            <div>
                <input type="text"
                       name=name
                       id="task-name">
            </div>

            <div>
                <button type="submit">
                    Add Task
                </button>
            </div>
        </form>
        {{--        hien thi tasks da co--}}
        @if(count($tasks_in_wed)>0)
            <div>Current Task</div>
            <div>
                <table>
                    <thead>
                    <td>Task</td>
                    <td>&nbsp;</td>
                    </thead>
                    <tbody>
                    @foreach($tasks_in_wed as $task)
                        <tr>
                            <td>
                                <div>{{$task->name}}</div>
                            </td>
                            <td>
                            <form action={{route('delete_task',[$task->id])}} method="post">
                                {{csrf_field()}}
                                {{method_field('DELETE')}}
                                <button >Delete Task</button>
                                <input type="hidden" name="_method" value="DELETE">
                            </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        @endif

    </div>
@endsection
