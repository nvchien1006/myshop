@extends('admin_layout')

@section('admin_content')
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                Sửa sản phẩm
            </header>
            <?php
            $mgs = Session::get('message');
            if ($mgs) {
                echo '<span class ="login_error">' . $mgs . '</span>';
                Session::put('message', null);
            }
            ?>
            <div class="panel-body">
                <div class="position-center">
                    @foreach($productInfor as $key=>$product)
                    <form role="form" enctype="multipart/form-data" action="{{URL::to('/updateProduct/'.$product->id)}}" method="post">
                        {{csrf_field()}}
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên sản phẩm</label>
                                <input name="product_name" type="text" class="form-control"
                                       value="{{$product->product_name}}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Giá sản phẩm</label>
                                <input name="product_price" type="text" class="form-control"
                                       value="{{$product->product_price}}"
                                >
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Ảnh sản phẩm</label>
                                <input name="product_img" type="file" class="form-control"
                                       value="{{$product->product_img}}"
                                >
                                <img src="{{URL::asset('uploads/product/'.$product->product_img)}}"
                                     width="100px" height="100px"
                                     alt="Images">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Mô tả sản phẩm</label>
                                <textarea name="product_desc" style="resize: none" rows="8" class="form-control"
                                >{{$product->product_des}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Nội dung sản phẩm</label>
                                <textarea name="product_content" style="resize: none" rows="8" class="form-control"
                                >{{$product->product_content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Danh mục sản phẩm</label>
                                <div class="checkbox">
                                    <select name="brand_id" class="form-control input-sm m-bot15">
                                        @foreach($brands as $key => $brand)
                                            @if($brand->id==$product->brand_id )
                                                <option selected value="{{$brand->id}}">{{$brand->brand_name}}</option>--}}
                                            @else
                                                <option value="{{$brand->id}}">{{$brand->brand_name}}</option>--}}
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Thương hiệu</label>
                                <div class="checkbox">
                                    <select name="cate_id" class="form-control input-sm m-bot15">
                                        @foreach($cates as $key => $cate)
                                            @if($cate->id==$product->category_id )
                                                <option selected value="{{$cate->id}}">{{$cate->category_name}}</option>--}}
                                            @else
                                                <option value="{{$cate->id}}">{{$cate->category_name}}</option>--}}
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        @endforeach
                        <button type="submit" name="category_add" class="btn btn-info">Cập nhật sản phẩm</button>
                    </form>
                </div>

            </div>
        </section>
    </div>
@endsection
