<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller{
    //
    public function index()
    {
        return view('pages.home');
    }

    public function search(Request $request)
    {
        $keyword = $request->keyword;
        $cates = \DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = \DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        $search_product = \DB::table('tbl_product')->where('product_name','like','%'.$keyword.'%')->get();
        return view('font_end_search') ->with('cates',$cates)
            ->with('brands',$brands)
            ->with('search_product',$search_product)
            ;
    }
}
