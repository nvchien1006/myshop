<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class ProductController extends Controller
{
    public function addProduct()
    {
        AdminController::authLogin();
        $cate_product = DB::table('tbl_category_product')->orderBy('id', 'desc')->get();
        $brand_product = DB::table('tbl_brand')->orderBy('id', 'desc')->get();
        return view('admin_product_add')
            ->with('cates', $cate_product)
            ->with('brands', $brand_product);
    }

    public function saveProduct(Request $request)
    {
        AdminController::authLogin();
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['product_price'] = $request->product_price;
        $data['product_des'] = $request->product_desc;
        $data['product_content'] = $request->product_content;
        $data['brand_id'] = $request->brand_id;
        $data['category_id'] = $request->cate_id;
        $data['product_status'] = $request->product_status;

        $img = $request->file('product_img');
        if ($img) {
            $newImg = $this->saveImg($img);
            $data['product_img'] = $newImg;
        } else {
            $data['product_img'] = '';
        }
        DB::table('tbl_product')->insert($data);
        Session::put('message', "Thêm sản phẩm thành công");
        return Redirect::to('/admin_add_product');

    }


    public function allProduct()
    {
        AdminController::authLogin();
        //getdata
        $all_product = DB::table('tbl_product')
            ->join('tbl_category_product', 'tbl_category_product.id', '=', 'tbl_product.category_id')
            ->join('tbl_brand', 'tbl_brand.id', '=', 'tbl_product.brand_id')
            ->select('tbl_product.id AS product_id', 'tbl_product.*', 'tbl_category_product.*', 'tbl_brand.*')
            ->orderBy('tbl_product.id', 'desc')
            ->get();
//        echo '<pre>';
//               print_r($all_product);
//        echo '</pre>';
        return view('admin_product_all')->with('products', $all_product);
    }

    public function deleteProduct($product_id)
    {
        AdminController::authLogin();
        DB::table('tbl_product')
            ->where('id', $product_id)
            ->delete();
        Session::put('messege', 'Xóa sản phẩm thành công!');
        return Redirect::to('/admin_all_product');
    }

    public function acProduct($productID)
    {
        AdminController::authLogin();
        DB::table('tbl_product')->where('id', $productID)->update(['product_status' => 1]);
        \Session::put('messege', 'Kích hoạt thành công');
        return \Redirect::to('/admin_all_product');
    }

    public function unacProduct($productID)
    {
        AdminController::authLogin();
        \DB::table('tbl_product')->where('id', $productID)->update(['product_status' => 0]);
        \Session::put('messege', 'Hủy kích hoạt thành công');
        return \Redirect::to('/admin_all_product');

    }

    public function editProduct($productId)
    {
        AdminController::authLogin();
        $productInfor = DB::table('tbl_product')->where('id', $productId)->get();
        $cates = DB::table('tbl_category_product')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->orderBy('id', 'desc')->get();
        return view('admin_product_edit')
            ->with('cates', $cates)
            ->with('brands', $brands)
            ->with('productInfor', $productInfor);
    }

    public function updateProduct(Request $request, $productID)
    {
        AdminController::authLogin();
        $data = array();
        $data['product_name'] = $request->product_name;
        $data['product_price'] = $request->product_price;
        $img = $request->file('product_img');
        if ($img) {
            $t = $this->saveImg($img);

            if (!$t) {
                $t = '';
            }
            $data['product_img'] = $t;
        }
        $data['product_des'] = $request->product_desc;
        $data['product_content'] = $request->product_content;
        $data['category_id'] = $request->cate_id;
        $data['brand_id'] = $request->brand_id;

        DB::table('tbl_product')->where('id', $productID)->update($data);
        Session::put('messege', 'Cập nhật sản phẩm thành công');
        return Redirect::to('/admin_all_product');
    }

    /**
     * @param $img
     * @return string
     */
    public function saveImg($img): string
    {
        AdminController::authLogin();
        $imgName = $img->getClientOriginalName();
        $imgNameTrue = current(explode(".", $imgName));
        $newImg = $imgNameTrue . rand(0, 100) . '.' . $img->getClientOriginalExtension();
        $img->move(public_path('uploads/product'), $newImg);
        return $newImg;
    }

//end admin

    public function showDetailProduct($product_id)
    {
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        $detail_product = DB::table('tbl_product')
            ->select('*', 'tbl_product.id AS product_id', 'tbl_category_product.id AS category_id')
            ->where('tbl_product.id', '=', $product_id)
            ->join('tbl_category_product', 'tbl_category_product.id', '=', 'tbl_product.category_id')
            ->join('tbl_brand', 'tbl_brand.id', '=', 'tbl_product.brand_id')
            ->get();

        foreach ($detail_product as $key => $value) {
            $cate_id = $value->category_id;
        }
        $relate_product = DB::table('tbl_product')
            ->select("*", 'tbl_product.id AS product_id')
            ->where('tbl_product.category_id', '=', $cate_id)
            ->whereNotIn('tbl_product.id', [$product_id])
            ->join('tbl_category_product', 'tbl_category_product.id', '=', 'tbl_product.category_id')
            ->join('tbl_brand', 'tbl_brand.id', '=', 'tbl_product.brand_id')
            ->get();

        return view('fontend_productdetails')->with('cates', $cates)
            ->with('detail_product', $detail_product)
            ->with('brands', $brands)
            ->with('relate_products', $relate_product);

    }

}
