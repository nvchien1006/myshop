<?php

namespace App\Http\Controllers;

use DB;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Redirect;


class CartController extends Controller
{
    //
    public function addToCard(Request $request){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
//        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();

        $product_quati = $request->quati;
        $product_id = $request->product_id_hide;
        $produc_infor = DB::table('tbl_product')->where('tbl_product.id', '=', $product_id)->first();

        $cart['id'] = $product_id;
        $cart['qty'] = $product_quati;
        $cart['name'] = $produc_infor->product_name;
        $cart['price'] = (float)$produc_infor->product_price;
        $cart['weight'] = 1;
        $cart['options']['image'] = $produc_infor->product_img;

        Cart::add($cart);

//        echo '<pre>';
//               print_r($produc_infor);
//        echo '</pre>';
        return Redirect::to('/showcart');
    }

    public function showcart()
    {
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        return view('cart.cart_item')
            ->with('cates',$cates)
            ->with('brands',$brands)
            ;
    }

    public function deleteCard($rowId){
        Cart::update($rowId, 0);
        return Redirect::to('/showcart');

    }

    public function updateCard(Request $request){
        $quatity = $request->quatity;
        $rowId = $request->row_id;
        Cart::update($rowId, $quatity);
        return Redirect::to('/showcart');


    }
}
