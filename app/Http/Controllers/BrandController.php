<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    //

    public function addBrand(){
        AdminController::authLogin();
        return view('admin_brand_add');
    }

    public function allBrand(){
        AdminController::authLogin();
        $allBrand = \DB::table('tbl_brand')->get();
        $manager_brand = view('admin_brand_all')->with('allBrand',$allBrand);
        return view('admin_layout')->with('$admin_brand_all', $manager_brand);
    }

    public function editBrand($brandId){
        //id > data
        AdminController::authLogin();
        $brandInfor = DB::table('tbl_brand')->where('id', $brandId)->get();
        return view('admin_brand_edit')->with('brandInfor', $brandInfor);
    }

    public function deleteBrand($brand_id){
        AdminController::authLogin();
        \DB::table('tbl_brand')->where('id', $brand_id)->delete();
        \Session::put('messege', 'Xóa nhãn hiệu thành công!');
        return \Redirect::to(route_brand_all);
    }

    public function saveBrand(Request $request){
        AdminController::authLogin();
        $data = array();
        $data['brand_name'] = $request->brand_name;
        $data['brand_des'] = $request->brand_desc;
        $data['brand_status'] = $request->brand_status;

        \DB::table('tbl_brand')->insert($data);
        \Session::put('message', "Thêm thương hiệu thành công");
        return \Redirect::to('/admin_add_brand');

    }

    public function updateBrand(Request $request, $brandId){
        AdminController::authLogin();
        $data = array();
        $data['brand_name'] = $request->brand_name;
        $data['brand_des'] = $request->brand_desc;
        DB::table('tbl_brand')->where('id', $brandId)->update($data);
        \Session::put('messege','Cập nhật sản phẩm thành công');
        return \Redirect::to('/brand_all');
    }

    public function acBrand($brandId)
    {
        AdminController::authLogin();
        \DB::table('tbl_brand')->where('id', $brandId)->update(['brand_status' => 1]);
        \Session::put('messege', 'Kích hoạt thành công');
        return \Redirect::to('/brand_all');
    }

    public function unacBrand($brandId)
    {
        AdminController::authLogin();
        \DB::table('tbl_brand')->where('id', $brandId)->update(['brand_status' => 0]);
        \Session::put('messege', 'Hủy kích hoạt thành công');
        return \Redirect::to('/brand_all');

    }

    //end admin

    public function showBrands($nhanhang_id){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        $brands_by_id = DB::table('tbl_product')
            ->where('tbl_product.brand_id', '=',$nhanhang_id)
            ->join('tbl_brand', 'tbl_product.brand_id', '=', 'tbl_brand.id')
            ->select('tbl_product.id AS product_id','tbl_brand.id AS brand_id','tbl_brand.*','tbl_product.*')
            ->get();
        return view('brands.brand_home')
            ->with('cates',$cates)
            ->with('brands',$brands)
            ->with('brands_by_id',$brands_by_id);
    }

}
