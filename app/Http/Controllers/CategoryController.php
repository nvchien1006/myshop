<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Redirect;

class CategoryController extends Controller
{
    //
    public function addCategoryProduct()
    {
        AdminController::authLogin();
        return view('admin_product_create');
    }

    public function allCategoryProduct()
    {
        AdminController::authLogin();
        $allCategoryProduct = \DB::table('tbl_category_product')->get();
        $manager_category = view('admin_products_all')->with('all_CategoryProduct', $allCategoryProduct);
        return view('admin_layout')->with('$admin_products_all', $manager_category);
    }

    public function saveProduct(Request $request)
    {
        AdminController::authLogin();
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['category_desc'] = $request->category_desc;
        $data['category_status'] = $request->category_status;
        \DB::table('tbl_category_product')->insert($data);

        \Session::put('message', "Thêm danh mục sản phẩm thành công");
        return \Redirect::to('admin_add_category_product');
    }

    public function showProductId($categori_id)
    {
        AdminController::authLogin();
        \DB::table('tbl_category_product')->where('id', $categori_id)->update(['category_status' => 1]);
        \Session::put('messege', 'Kích hoạt thành công');
        return \Redirect::to('/admin_all_category_product');
    }

    public function hideProductId($categori_id)
    {
        AdminController::authLogin();
        \DB::table('tbl_category_product')->where('id', $categori_id)->update(['category_status' => 0]);
        \Session::put('messege', 'Hủy kích hoạt thành công');
        return \Redirect::to('/admin_all_category_product');

    }

    public function deleteProduct($category_id)
    {
        AdminController::authLogin();
        \DB::table('tbl_category_product')->where('id', $category_id)->delete();
        \Session::put('messege  ', 'Xóa danh mục thành công!');
        return \Redirect::to('/admin_all_category_product');
    }

    public function editProductId($category_id)
    {
        AdminController::authLogin();
        $edit_category_product = \DB::table('tbl_category_product')->where('id', $category_id)->get();
        $manager_cate_product = view('admin_product_edit')->with('edit_cate', $edit_category_product);
        return view('admin_layout')->with('admin_product_edit', $manager_cate_product);

    }

    public function updateProductId(Request $request, $category_id)
    {
        AdminController::authLogin();
        $data = array();
        $data['category_name'] = $request->category_name;
        $data['category_desc'] = $request->category_desc;

        \DB::table('tbl_category_product')->where('id', $category_id)
            ->update($data);
        \Session::put('messege', 'Cập nhật sản phẩm thành công');
        return \Redirect::to('/admin_all_category_product');
    }

    //end admin
    public function showCategoryHome($danhmuc_id){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        $cate_by_id = DB::table('tbl_product')
            ->where('tbl_product.category_id', '=',$danhmuc_id)
            ->join('tbl_category_product', 'tbl_product.category_id', '=', 'tbl_category_product.id')
            ->select('tbl_product.id AS product_id','tbl_category_product.id AS category_id','tbl_category_product.*','tbl_product.*')
            ->get();
        return view('categorys.category_home')
            ->with('cates',$cates)
            ->with('brands',$brands)
            ->with('cate_by_id',$cate_by_id);
    }

}
