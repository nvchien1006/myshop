<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Redirect;
use Session;

class AdminController extends Controller{
    public static function authLogin()
    {
        $adminID = Session::get('admin_id');
        if (!$adminID) {
            return Redirect::to('/admin_login')->send();
        }
    }

    public function dashboard()
    {
        return view('admin_dashbroad');
    }

    public function loginAdmin()
    {
        return view('admin_login');


    }

    public function admin_dashboard(Request $request)
    {
        $admin_email = $request->admin_mail;
        $admin_pass = $request->admin_password;
        $result = DB::table('admin')->where('admin_mail', $admin_email)
            ->where('admin_password', $admin_pass) ->first();
        if ($result) {
            Session::put('admin_name',$result->admin_name);
            Session::put('admin_id',$result->id);
            return \Redirect::to('/dashbroad');
        }else{
            Session::put('message', 'Mat khau hoac Tai khoan bi sai. Hay nhap lai');
            return \Redirect::to('/admin_login');
        }
    }

    public function admin_logout()
    {
        $this->authLogin();
        Session::put('admin_name',null);
        Session::put('admin_id',null);
        return \Redirect::to('/admin_login');
    }

}
