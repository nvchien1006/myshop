<?php

namespace App\Http\Controllers;

use DB;
use Doctrine\DBAL\Schema\Table;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Redirect;
use Session;

class CheckoutController extends Controller{
    public const HAND_CASH = 1;

    public static function loginCheckOut(){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        return view('checkout.login_checkout')
            ->with('cates',$cates)
            ->with('brands',$brands)
            ;
    }

    public function addCustomer(Request $request){
        $customer = array();
        $customer['customer_name'] = $request->name;
        $customer['customer_email'] = $request->email;
        $customer['customer_password'] = $request->pass;
        $customer['customer_phone'] = $request->phone;
        $id = DB::table('tbl_customer')->insertGetId($customer);
        Session::put('customer_id',$id);
        Session::put('customer_name',$request->name);
        Redirect::to('/check_out');
    }

    public function checkOut(){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        return view('checkout.checkout')
            ->with('cates',$cates)
            ->with('brands',$brands)
            ;

    }

    public function saveCheckOutcustomer(Request $request){
        $shipping_infor = array();
        $shipping_infor['shiping_name'] = $request->shiping_name;
        $shipping_infor['shiping_address'] = $request->shiping_address;
        $shipping_infor['shiping_phone'] = $request->shiping_phone;
        $shipping_infor['shiping_email'] = $request->shiping_mail;
        $shipping_infor['shipping_note'] = $request->shiping_note;

        $shippingID = DB::table('tbl_shipping') ->insertGetId($shipping_infor);
        Session::put('shipping_id',$shippingID);

        return Redirect::to('/payment');
    }

    public function payment(){
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();
        return view('checkout.pament')->with('cates',$cates) ->with('brands',$brands);
    }

    public function logout(){
        Session::flush();
        return Redirect::to('/login-checkout');
    }

    public function loginCustomer(Request $request){
        $email = $request->mail;
        $pas = $request->pas;
        $result = DB::table('tbl_customer')
            ->where('customer_email', $email)
            ->where('customer_password', $pas)
            ->first();
        if($result){
            Session::put('customer_id',$result->id);
//            Session::put('customer_name',$request->name);
            return Redirect::to('/check_out');
        }else{
            return Redirect::to('/login-checkout');
        }
    }
    public function orderPlace(Request $request){
        //
        $pay = array();
        $method = $request->input('payment_option');
        if($method==NULL){
            $method = self::HAND_CASH;
        }
        $pay['payment_method'] =$method;
        $pay['pament_status'] ="Đang chờ sử lý";
        $payment_id = DB::table('tbl_payment')->insertGetId($pay);


        $order = array();
        $order['customer_id'] =Session::get('customer_id');
        $order['payment_id'] =$payment_id;
        $order['shipping_id'] =Session::get('shipping_id');
        $order['order_total'] =Cart::total();
        $order['order_status'] ="Đang chờ sử lý";
        $order_id = DB::table('tbl_order')->insertGetId($order);

        $carts = Cart::content();
        foreach ($carts as $cart){
            $order_detail = array();
            $order_detail['order_id'] = $order_id;
            $order_detail['product_id'] = $cart->id;
            $order_detail['product_name'] = $cart->name;
            $order_detail['product_price'] = $cart->price;
            $order_detail['product_sales_quality'] = $cart->qty;
            DB::table('tbl_order_detail')->insert($order_detail);
        }

//        if ($pay['payment_method'] == self::HAND_CASH) {
//
//        }
        return $this->showResult();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResult()
    {
        Cart::destroy();
        $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id', 'desc')->get();
        $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id', 'desc')->get();

        return view('checkout.pament_sessec')
            ->with('cates', $cates)
            ->with('brands', $brands);
    }

    public function managerCarts(){
        AdminController::authLogin();
        $all_orders = DB::table('tbl_order')
            ->join('tbl_customer', 'tbl_order.customer_id', '=', 'tbl_customer.id')
            ->select('tbl_order.*', 'tbl_customer.customer_name')
            ->orderBy('tbl_order.order_id','desc')
            ->get();
        return view('admin_carts_all')->with('all_orders',$all_orders);
    }

    public function viewOrder(Request $request,$order_id){
        AdminController::authLogin();
        $order_details = DB::table('tbl_order')
            ->join('tbl_order_detail', 'tbl_order.order_id', '=', 'tbl_order_detail.order_id')
            ->join('tbl_customer', 'tbl_order.customer_id', '=', 'tbl_customer.id')
            ->join('tbl_shipping', 'tbl_order.shipping_id', '=', 'tbl_shipping.shiping_id')
            ->where('tbl_order.order_id', '=', $order_id)
            ->first();
        return view('admin_view_cart')->with('order_detail',$order_details);
    }
}
