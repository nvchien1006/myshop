<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


define("tbl_brand", "tbl_brand");
define("brand_name", "brand_name");
define("brand_des", "brand_des");
define("brand_status", "brand_status");

class CreateTblBrand extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        Schema::create(tbl_brand, function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->string(brand_name);
            $table->text(brand_des);
            $table->integer(brand_status);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(tbl_brand);
    }
}
