<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//font end

//1
use App\Http\Controllers\BrandController;

//home
Route::get('/', function () {
    $cates = DB::table('tbl_category_product')->where('category_status', '1')->orderBy('id','desc')->get();
    $brands = DB::table('tbl_brand')->where('brand_status', '1')->orderBy('id','desc')->get();
    $products = DB::table('tbl_product')->where('product_status', '1')->orderBy('id','desc')->get();
    return view('pages.home')
        ->with('cates',$cates)
        ->with('brands',$brands)
        ->with('products',$products);
});

//danhmuc san pham
Route::get('/danhmuc_sanpham/{danhmuc_id}','CategoryController@showCategoryHome');
Route::get('/nhanhang_sanpham/{nhanhang_id}','BrandController@showBrands');

Route::get('/product_detail/{product_id}','ProductController@showDetailProduct');
Route::post('/save-cart','CartController@addToCard');
Route::get('/showcart','CartController@showcart');
Route::get('/delete_card/{rowId}', 'CartController@deleteCard');
Route::post('/update-cart-quality', 'CartController@updateCard');
Route::get('/login-checkout', 'CheckoutController@loginCheckOut');
Route::post('/login-customer', 'CheckoutController@loginCustomer');
Route::get('/logout', 'CheckoutController@logout');
Route::post('/add_customer', 'CheckoutController@addCustomer');
Route::get('/check_out', 'CheckoutController@checkOut');
Route::post('/save_checkout_customer', 'CheckoutController@saveCheckOutcustomer');
Route::get('/payment', 'CheckoutController@payment');
Route::post('/order-place', 'CheckoutController@orderPlace');

Route::post('/search', 'HomeController@search');

//back end
//1.login/logout

//login view
Route::get('/admin_login', 'AdminController@loginAdmin');
//login request
Route::post('/admin_dashboard', 'AdminController@admin_dashboard');
//main view
Route::get('/dashbroad','AdminController@dashboard');
//logout request
Route::get('/admin_logout','AdminController@admin_logout');

//2.product
Route::get('/admin_add_category_product', 'CategoryController@addCategoryProduct');
Route::get('/admin_all_category_product', 'CategoryController@allCategoryProduct');
Route::get('/update_category/{categoti_id}', 'CategoryController@editProductId');
Route::get('/delete_product/{categoti_id}', 'CategoryController@deleteProduct');

Route::get('/acti_status_product/{categori_id}', 'CategoryController@showProductId');
Route::get('/unacti_status_product/{categori_id}', 'CategoryController@hideProductId');

Route::post('/save_product', 'CategoryController@saveProduct');
Route::post('/update_category_submit/{categori_id}', 'CategoryController@updateProductId');

//3 brand
define("route_brand_save", "/save_brand");
define("route_brand_all", "/brand_all");

Route::get('/admin_add_brand', 'BrandController@addBrand');
Route::get(route_brand_all, 'BrandController@allBrand');
Route::get('/deleteBrand/{brand_id}', 'BrandController@deleteBrand');
Route::get('/editBrand/{brandId}', 'BrandController@editBrand');

Route::get('/acVisible/{brandId}', 'BrandController@acBrand');
Route::get('/unacVisible/{brandId}', 'BrandController@unacBrand');

Route::post(route_brand_save, 'BrandController@saveBrand');
Route::post('/updateBrand/{brandId}', 'BrandController@updateBrand');

// san pham
Route::get('/admin_add_product', 'ProductController@addProduct');
Route::get('/admin_all_product', 'ProductController@allProduct');
Route::get('/deleteProduct/{product_id}', 'ProductController@deleteProduct');
Route::get('/editProduct/{productId}', 'ProductController@editProduct');

//
Route::get('/acVisible_sanpham/{productID}', 'ProductController@acProduct');
Route::get('/unacVisible_sanpham/{productID}', 'ProductController@unacProduct');

//
Route::post('/product_save', 'ProductController@saveProduct');
Route::post('/updateProduct/{productID}', 'ProductController@updateProduct');

//funtion: quản lý đơn hàng
Route::get('/manager_carts', 'CheckoutController@managerCarts');
Route::get('/view_order/{order_id}', 'CheckoutController@viewOrder');

